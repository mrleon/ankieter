﻿angular.module("umbraco")
    .controller("afxSkipLinks",
        function ($rootScope, $scope, dialogService) {

            if (!$scope.model.value) {
                $scope.model.value = [];
            }
            $scope.newID = '';
            $scope.newTekst = '';

            $scope.hasError = false;
            $scope.limit = $scope.model.config.limit || 500;

            $scope.currentEditLink = _.where($scope.model.value, { edit: true })[0] || null;
            $scope.currentEditLinkCopy = $scope.currentEditLink ? angular.extend({}, $scope.currentEditLink) : null;

            $scope.edit = function (editedLink) {
                for (var i = 0; i < $scope.model.value.length; i++) {
                    $scope.model.value[i].edit = false;
                }
                editedLink.edit = true;
                $scope.currentEditLink = editedLink;
                $scope.currentEditLinkCopy = angular.extend({}, editedLink);
            };

            $scope.cancelEdit = function (editedLink) {
                editedLink.edit = false;
                $scope.currentEditLink = null;
                $scope.currentEditLinkCopy = null;
            };

            $scope.saveEdit = function (editedLink) {
                var oldOrder = $scope.currentEditLink.order;
                var newOrder = $scope.currentEditLinkCopy.order;
                angular.extend($scope.currentEditLink, $scope.currentEditLinkCopy);
                if (oldOrder != newOrder) {
                    linkOrderModified($scope.currentEditLink, oldOrder, newOrder);
                    updateSelects();
                }
                $scope.cancelEdit(editedLink);
            };

            $scope.delete = function (editedLink) {
                var deletedLink = $scope.model.value.splice($scope.model.value.indexOf(editedLink), 1)[0];
                linkOrderDeleted(deletedLink);
                updateSelects();
            };

            $scope.add = function ($event) {
                if ($scope.ID == "") {
                    $scope.hasError = true;
                } else {

                    var newExtLink = new function () {
                        this.ID = $scope.newID;
                        this.Tekst = $scope.newTekst;

                        this.edit = false;
                        this.order = $scope.newOrder || 1


                    };
                    $scope.model.value.push(newExtLink);
                    linkOrderAdded(newExtLink);

                    $scope.newID = '';
                    $scope.newTekst = '';

                    updateSelects();
                }

                $event.preventDefault();
            };



            // order
            $scope.newOrders = [];
            $scope.oldOrders = [];
            $scope.newOrder = 1;
            updateSelects();

            function updateSelects() {
                $scope.newOrders = [];
                $scope.oldOrders = [];
                for (var i = 1; i < $scope.model.value.length + 2; i++) {
                    $scope.newOrders.push(i);
                    $scope.oldOrders.push(i);
                }
                $scope.oldOrders.pop();
            }

            function linkOrderModified(modifiedLink, oldOrder, newOrder) {

                if (oldOrder < newOrder) {  // jezeli zostala ustawiona wyzsza kolejnosc

                    for (var i = 0; i < $scope.model.value.length; i++) {

                        var current = $scope.model.value[i];

                        if (current == modifiedLink) continue;

                        if ((oldOrder < current.order) && (current.order <= newOrder))
                            current.order--;
                    }

                } else if (oldOrder > newOrder) {   // jezeli zostala ustawiona nizsza kolejnosc

                    for (var i = 0; i < $scope.model.value.length; i++) {

                        var current = $scope.model.value[i];

                        if (current == modifiedLink) continue;

                        if ((newOrder <= current.order) && (current.order < oldOrder))
                            current.order++;
                    }

                }

            }

            function linkOrderDeleted(deletedLink) {
                for (var i = 0; i < $scope.model.value.length; i++) {

                    var current = $scope.model.value[i];

                    if (current.order > deletedLink.order)
                        current.order--;
                }
            }

            function linkOrderAdded(newLink) {
                for (var i = 0; i < $scope.model.value.length; i++) {
                    var current = $scope.model.value[i];

                    if (current == newLink) continue;

                    if (current.order >= newLink.order)
                        current.order++;
                }
            }

        });