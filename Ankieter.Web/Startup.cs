﻿using Owin;

namespace TestWeb
{

    //[assembly: OwinStartup(typeof(Ankieter.TestWeb.App_Start.Startup))]
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
