﻿angular.module('ankieter', [])
.controller('Controller', ['$scope', function ($scope) {

    $scope.formCounter = 2;


    $scope.setNewForm = function (type) {
        $scope.newForm = type;
    };

    $scope.addForm = function (type) {
        $scope.formCounter++;
        switch (type) {
            case 'radio': {
                addRadioForm();
                break;
            }
            case 'text': {
                addTextForm();
                break;
            }
            case 'checkbox': {
                addCheckboxForm();
                break;
            }
            case 'label': {
                addLabelForm();
                break;
            }
            case 'multilineText': {
                addMultilineTextForm();
                break;
            }
            case 'list': {
                addListForm();
                break;
            }
            case 'grid': {
                addGridForm();
                break;
            }
        }

    };


    function addRadioForm() {
        var form =
        {
            "title": "",
            "description": "",
            "type": "radio",
            "required": false,
            "index": $scope.formCounter,
            "options": [
                {
                    "value": "",
                    "edit": false,
                }
            ],
        }
        $scope.forms.push(form);
    }
    function addCheckboxForm() {
        var form =
        {
            "title": "",
            "description": "",
            "type": "checkbox",
            "required": false,
            "index": $scope.formCounter,
            "options": [
                {
                    "value": "",
                    "edit": false,
                }
            ],
        }
        $scope.forms.push(form);
    }
    function addTextForm() {
        var form =
        {
            "title": "",
            "description": "",
            "type": "text",
            "required": false,
            "index": $scope.formCounter,
            "value": "",
        };
        $scope.forms.push(form);
    }
    function addLabelForm() {
        var form =
        {
            "title": "",
            "description": "",
            "type": "label",
            "index": $scope.formCounter,
        };
        $scope.forms.push(form);
    }
    function addMultilineTextForm() {
        var form =
        {
            "title": "",
            "description": "",
            "type": "multiline",
            "required": false,
            "index": $scope.formCounter,
            "value": "",
        };
        $scope.forms.push(form);
    };

    $scope.removeForm = function (form) {
        for (var i = form.index; i < $scope.forms.length; i++) {
            $scope.forms[i].index--;
        }

        var index = $scope.forms.indexOf(form);
        $scope.forms.splice(index, 1);
    };








    $scope.forms = [
        {
            "title": "Co wolisz?",
            "description": "powiedz mi",
            "type": "radio",
            "required": false,
            "index": 0,
            "options": [
                {
                    "value": "Facet",
                    "edit": false,
                },
                {
                    "value": "Kobieta",
                    "edit": false,
                },
                {
                    "value": "Nic",
                    "edit": false,
                }
            ],

        },
        {
            "title": "tes sdsdf sdf sdfs dfsd fsdf sdf sd?",
            "description": "powiedz mi",
            "type": "checkbox",
            "required": false,
            "index": 1,
            "options": [
                {
                    "value": "Facet",
                    "edit": false,
                },
                {
                    "value": "Kobieta",
                    "edit": false,
                },
                {
                    "value": "Nic",
                    "edit": false,
                }
            ],

        },
    ];

    $scope.options = [
     {
         "value": "radio",
     },
     {
         "value": "text",
     },
     {
         "value": "checkbox",
     },
     {
         "value": "label",
     },
     {
         "value": "multilineText",
     },
     {
         "value": "grid",
     },
    ];


}])
.directive('checkboxForm', ['$sce', function ($sce) {

    return {
        restrict: 'E',
        scope: {
            form: '=',
            remove: '&',
        },
        templateUrl: $sce.trustAsResourceUrl('checkboxForm.html'),
        link: function (scope, element, attrs) {
            scope.removeOption = function (option, form) {
                var index = form.options.indexOf(option);
                form.options.splice(index, 1);
            };
            scope.addOption = function (form) {
                var option = {
                    "value": "",
                    "edit": false,
                };
                form.options.push(option);
            };
        }
    }


}])
.directive('radioForm', ['$sce', function ($sce) {

    return {
        restrict: 'E',
        scope: {
            form: '=',
            remove: '&',
        },
        templateUrl: $sce.trustAsResourceUrl('radioForm.html'),
        link: function (scope, element, attrs) {

            scope.removeOption = function (option, form) {
                var index = form.options.indexOf(option);
                form.options.splice(index, 1);
            };
            scope.addOption = function (form) {
                var option = {
                    "value": "",
                    "edit": false,
                };
                form.options.push(option);
            };
        }
    }


}])
.directive('textForm', ['$sce', function ($sce) {

    return {
        restrict: 'E',
        scope: {
            form: '=',
            remove: '&',
        },
        templateUrl: $sce.trustAsResourceUrl('textForm.html'),
        link: function (scope, element, attrs) {

        }
    }
}])
.directive('multilineForm', ['$sce', function ($sce) {

    return {
        restrict: 'E',
        scope: {
            form: '=',
            remove: '&',
        },
        templateUrl: $sce.trustAsResourceUrl('multilineForm.html'),
        link: function (scope, element, attrs) {

        }
    }
}]);