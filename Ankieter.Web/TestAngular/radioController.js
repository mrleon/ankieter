app.controller("radioController", function ($scope) {

    $scope.newItem = "";
    $scope.currentItem = "";

    $scope.delete = function (index) {
        $scope.model.splice(index, 1);
        for (var i = 0; i < $scope.model.length; i++) {
            $scope.model[i].edit = false;
        }
    };
    $scope.edit = function (index) {
        for (var i = 0; i < $scope.model.length; i++) {
            $scope.model[i].edit = false;
        }
        $scope.model[index].edit = true;
        $scope.currentItem = $scope.model[index].value;
    };
    $scope.cancel = function (index) {
        for (var i = 0; i < $scope.model.length; i++) {
            $scope.model[i].edit = false;
        }
        $scope.model[index].value = $scope.currentItem;
    };
    $scope.save = function (item) {
        if (!item.value)
            return;
        item.edit = false;
    };
    $scope.add = function () {
        if (!$scope.newItem)
            return;
        var item = {
            "value": $scope.newItem,
            "edit" : false,
        };
        $scope.model.push(item);
        $scope.newItem = "";
    };
    $scope.toTop = function (index) {
        var tmp = $scope.model[index];
        $scope.model[index] = $scope.model[index - 1];
        $scope.model[index - 1] = tmp;
    };
    $scope.toBottom = function (index) {
        var tmp = $scope.model[index];
        $scope.model[index] = $scope.model[index + 1];
        $scope.model[index + 1] = tmp;
    };



    $scope.model = [
        {
            "value": "Facet",
            "edit": false,
        },
        {
            "value": "Kobieta",
            "edit": false,
        },
        {
            "value": "Dziecko",
            "edit": false,
        },
    ];


});

