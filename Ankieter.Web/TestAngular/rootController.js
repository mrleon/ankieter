﻿var app = angular.module("ankieter", []);

app.controller("rootController", function ($rootScope, $scope) {
    $scope.title = "";
    $scope.description = "";
    $scope.newItem = "";
    $scope.divider = "---------------------------------------------------------------------------------------------------------------";
    $scope.formCounter = 4;


    $scope.setNewForm = function (type) {
        $scope.newForm = type;
    };

    $scope.addForm = function (type) {
        $scope.formCounter++;
        switch (type) {
            case 'radio': {
                addRadioForm();
                break;
            }
            case 'text': {
                addTextForm();
                break;
            }
            case 'checkbox': {
                addCheckboxForm();
                break;
            }
            case 'label': {
                addLabelForm();
                break;
            }
            case 'multilineText': {
                addMultilineTextForm();
                break;
            }
            case 'list': {
                addListForm();
                break;
            }
            case 'grid': {
                addGridForm();
                break;
            }
        }

    };
    $scope.removeForm = function (form) {
        for (var i = form.index; i < $scope.forms.length; i++) {
            $scope.forms[i].index--;
        }

        var index = $scope.forms.indexOf(form);
        $scope.forms.splice(index, 1);
    };

    $scope.addOption = function (form) {
        if (form.options[form.options.length - 1].value == "")
            return;

        var option = {
            "value": "",
            "edit": false,
        };
        form.options.push(option);
    };
    $scope.removeOption = function (option, form) {
        var index = form.options.indexOf(option);
        form.options.splice(index, 1);
    };

    $scope.addGridOption = function (form, isRow) {
        var option = {
            "value": "",
            "edit": false,
        };
        if (isRow) {
            form.row.push(option);
        }
        else {
            form.column.push(option);
        }

    };

    $scope.removeGridOption = function (form, option, isRow) {
        if (isRow) {
            var index = form.row.indexOf(option);
            form.row.splice(index, 1);
        }
        else {
            var index = form.column.indexOf(option);
            form.column.splice(index, 1);
        }
    }

    $scope.orderChanged = function (form, oldValue) {
        for (var i = 0; i < $scope.forms.length; i++) {
            if ($scope.forms[i] == form)
                continue;
            if (form.index < oldValue) {
                if ($scope.forms[i].index >= form.index && $scope.forms[i].index <= oldValue) {
                    $scope.forms[i].index++;
                }
            }
            else {
                if ($scope.forms[i].index <= form.index && $scope.forms[i].index >= oldValue) {
                    $scope.forms[i].index--;
                }
            }
        }
        console.log(form.title);
        $scope.forms.sort(function (a, b) { return a.index - b.index; });
    }




    $scope.forms = [
        {
            "title": "Co wolisz?",
            "description": "powiedz mi",
            "type": "radio",
            "required": false,
            "index": 1,
            "options": [
                {
                    "value": "Facet",
                    "edit": false,
                },
                {
                    "value": "Kobieta",
                    "edit": false,
                },
                {
                    "value": "Nic",
                    "edit": false,
                }
            ],

        },
        {
            "title": "nic nie chcę",
            "description": "opis inny",
            "type": "radio",
            "required": false,
            "index": 2,
            "options": [
                {
                    "value": "Facetinny",
                    "edit": false,
                },
                {
                    "value": "Kobieta inna",
                    "edit": false,
                },
                {
                    "value": "Nic takiego",
                    "edit": false,
                }
            ],
        },
        {
            "title": "checkxbosdusd",
            "description": "opis checkboxa",
            "type": "checkbox",
            "required": false,
            "index": 3,
            "options": [
                {
                    "value": "jeden",
                    "edit": false,
                },
                {
                    "value": "dwa",
                    "edit": false,
                },
                {
                    "value": "trzy",
                    "edit": false,
                },
            ],
        },
        {
            "title": "text test",
            "description": "opis tesxtu",
            "type": "text",
            "required": false,
            "index": 4,
            "value": "",
        },
    ];


    $scope.options = [
        {
            "value": "radio",
        },
        {
            "value": "text",
        },
        {
            "value": "checkbox",
        },
        {
            "value": "label",
        },
        {
            "value": "multilineText",
        },
        {
            "value": "list",
        },
        {
            "value": "grid",
        },
    ];


    function addRadioForm() {
        var form =
        {
            "title": "",
            "description": "",
            "type": "radio",
            "required": false,
            "index": $scope.formCounter,
            "options": [
                {
                    "value": "",
                    "edit": false,
                }
            ],
        }
        $scope.forms.push(form);
    }
    function addCheckboxForm() {
        var form =
        {
            "title": "",
            "description": "",
            "type": "checkbox",
            "required": false,
            "index": $scope.formCounter,
            "options": [
                {
                    "value": "",
                    "edit": false,
                }
            ],
        }
        $scope.forms.push(form);
    }
    function addTextForm() {
        var form =
        {
            "title": "",
            "description": "",
            "type": "text",
            "required": false,
            "index": $scope.formCounter,
            "value": "",
        };
        $scope.forms.push(form);
    }
    function addLabelForm() {
        var form =
        {
            "title": "",
            "description": "",
            "type": "label",
            "index": $scope.formCounter,
        };
        $scope.forms.push(form);
    }
    function addMultilineTextForm() {
        var form =
        {
            "title": "",
            "description": "",
            "type": "multilineText",
            "required": false,
            "index": $scope.formCounter,
            "value": "",
        };
        $scope.forms.push(form);
    };
    function addListForm() {
        var form =
        {
            "title": "",
            "description": "",
            "type": "list",
            "required": false,
            "index": $scope.formCounter,
            "options": [
                {
                    "value": "",
                    "edit": false,
                }
            ],
        };
        $scope.forms.push(form);
    };
    function addGridForm() {
        var form =
        {
            "title": "",
            "description": "",
            "type": "list",
            "required": false,
            "index": $scope.formCounter,
            "column": [
                {
                    "value": "",
                    "edit": false,
                }
            ],
            "row": [
                {
                    "value": "",
                    "edit": false,
                }
            ],
        };
        $scope.forms.push(form);
    }





});

function h(e) {
    $(e).css({ 'height': 'auto', 'overflow-y': 'hidden' }).height(e.scrollHeight);
}
$('textarea').each(function () {
    h(this);
}).on('input', function () {
    h(this);
});