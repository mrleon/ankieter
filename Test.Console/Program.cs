﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace Tests
{
    class Program
    {
        public enum QuestionType
        {
            Radio,
            Checkbox,
            Text,
            MultilineText,

        }
        static void Main(string[] args)
        {
            Main3();
        }

        static void Main2(string[] args)
        {
            dynamic stuff = JArray.Parse("[{\"title\":\"Co wolisz?\",\"description\":\"powiedz mi\",\"type\":\"radio\",\"required\":false,\"index\":0,\"options\":[\"Facet\",\"Kobieta\",\"Nic\"]},{\"title\":\"tes sdsdf sdf sdfs dfsd fsdf sdf sd?\",\"description\":\"powiedz mi\",\"type\":\"checkbox\",\"required\":false,\"index\":1,\"options\":[\"Facet\",\"Kobieta\",\"Nic\"]},{\"title\":\"tesxt\",\"description\":\"asdasd\",\"type\":\"text\",\"required\":false,\"index\":3},{\"title\":\"multittext\",\"description\":\"asdsad\",\"type\":\"multiline\",\"required\":true,\"index\":4}]");
            try
            {
                foreach (var item in stuff)
                {
                    Console.WriteLine("*------------------------------------\n");
                    Console.WriteLine("Title: " + item.title.Value);
                    Console.WriteLine("Description: " + item.description.Value);
                    Console.WriteLine("Required: " + item.required.Value);
                    Console.WriteLine("Index: " + item.index.Value);
                    Console.WriteLine("Type: " + item.type.Value);
                    if (item.type.Value == "radio")
                    {
                        foreach (var option in item.options)
                        {
                            Console.WriteLine(option);
                        }

                    }
                    else if (item.type.Value == "checkbox")
                    {
                        foreach (var option in item.options)
                        {
                            Console.WriteLine(option);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw;
            }

            Console.ReadKey();
        }

        static void Main3()
        {
            const string text = @"[""jeden"", ""dwa"", ""trzy""]";

            dynamic json = JArray.Parse(text);
            foreach (var item in json)
            {
                Console.WriteLine(item.ToString());
            }

            Console.WriteLine(json.Count);


            Console.ReadKey();
        }
    }
}
