﻿using System.Web.Mvc;
using System.Web.Routing;

namespace TestWeb.App_Start
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "GetSurvey Full Url",
                url: "Survey/Get/{id}/{url}",
                defaults:
                    new { controller = "Survey", action = "Get", id = UrlParameter.Optional, url = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}