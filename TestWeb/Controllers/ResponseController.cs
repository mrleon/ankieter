﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using TestWeb.DAL;
using TestWeb.Infrastructure.Common;
using TestWeb.Infrastructure.Response.Add;
using TestWeb.Infrastructure.Response.Get;
using TestWeb.Models;
using TestWeb.ViewModels;

namespace TestWeb.Controllers
{
    public class ResponseController : Controller
    {
        private readonly ExDbContext _context;

        public ResponseController()
        {
            _context = System.Web.HttpContext.Current.GetOwinContext().GetUserManager<ExDbContext>();
        }


        [HttpPost]
        public ActionResult Add(int id, string content)
        {
            Debug.WriteLine(content);
            IAddResponseManager manager = new AddResponseManager(_context);
            var result = manager.TryAddResponse(id, content);
            switch (result.Result())
            {
                case Result.Success:
                    return Json(new { url = Url.Action("Thanks", "Response") });
                case Result.EndDateReached:
                    return Json(new { url = Url.Action("EndDateReached", "Response") });
                case Result.ResponsesLimitReached:
                    return Json(new { url = Url.Action("ResponsesLimitReached", "Response") });
                default:
                    return Json(new { url = Url.Action("Error", "Response") });
            }
        }

        [HttpGet]
        [Authorize]
        public ActionResult MySurveys()
        {
            var userId = User.Identity.GetUserId();

            var surveyList = _context.Questionnaires.Where(t => t.ExUserID == userId).ToList();

            var model = surveyList.Select(survey => new MySurveyViewModel
            {
                Title = survey.Title,
                Url = Url.Action("Get", "Survey", new { id = survey.ID, url = survey.Url }),
                IsActive = !IsTimeLimitReached(survey) && !IsResponsesLimitReached(survey),
                ResponsesAdded = survey.Responses.Count,
                ClosedAtDateTime = survey.LimitDateTime,
                ClosedAtResponsesNumber = survey.ResponsesLimit,
                CsvUrl = Url.Action("GetCsvReport", "Response", new { id = survey.ID }),
                ReportUrl = Url.Action("GetReport", "Response", new { id = survey.ID })
            }).ToList();

            return View(model);
        }

        [HttpGet]
        [Authorize]
        public ActionResult GetReport(int id)
        {
            var questionnaire = _context.Questionnaires.FirstOrDefault(t => t.ID == id);
            if (questionnaire == null || questionnaire.ExUserID != User.Identity.GetUserId()) // questionnaire exists and is bound to current user
            {
                return View("Error");
            }

            IGetResponsesManager manager = new GetResponsesManager(_context);
            var model = manager.GetSurveyResponses(id);
            if (model == null)
            {
                return View("Error");
            }

            return View(model);
        }

        [HttpGet]
        [Authorize]
        public ActionResult GetCsvReport(int id)
        {
            var questionnaire = _context.Questionnaires.FirstOrDefault(t => t.ID == id);
            if (questionnaire == null || questionnaire.ExUserID != User.Identity.GetUserId()) // questionnaire exists and is bound to current user
            {
                return View("Error");
            }

            IGetResponsesManager manager = new GetResponsesManager(_context);
            var model = manager.GetCsvReport(id);

            if (model != null)
            {
                
                var data = Encoding.UTF8.GetBytes(model.Content);
                var result = Encoding.UTF8.GetPreamble().Concat(data).ToArray();

                return File(result, model.ContentType, model.FileName);

            }
            return null;
        }






        #region add response views

        public ActionResult Thanks()
        {
            return View();
        }

        public ActionResult EndDateReached()
        {
            return View();
        }

        public ActionResult ResponsesLimitReached()
        {
            return View();
        }

        public ActionResult Error()
        {
            return View();
        }

        #endregion

        private static bool IsTimeLimitReached(Questionnaire questionnaire)
        {
            if (!questionnaire.LimitDateTime.HasValue)
            {
                return false;
            }

            return questionnaire.LimitDateTime.Value < DateTime.Now;
        }

        private static bool IsResponsesLimitReached(Questionnaire questionnaire)
        {
            if (!questionnaire.ResponsesLimit.HasValue)
            {
                return false;
            }
            return questionnaire.Responses.Count >= questionnaire.ResponsesLimit;
        }
    }
}