﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using TestWeb.DAL;
using TestWeb.Infrastructure.Common;
using TestWeb.Infrastructure.Survey;
using TestWeb.Infrastructure.Survey.Add;
using TestWeb.Infrastructure.Survey.Get;

namespace TestWeb.Controllers
{
    public class SurveyController : Controller
    {
        private readonly ExDbContext _context;

        public SurveyController()
        {
            _context = System.Web.HttpContext.Current.GetOwinContext().GetUserManager<ExDbContext>();
        }

        public SurveyController(ExDbContext context)
        {
            _context = context;
        }


        [Authorize]
        [HttpGet]
        public ActionResult Add()
        {
            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult Add(string title, string description, string content, string dateLimit, int responsesLimit)
        {
            IAddSurveyManager manager = new AddSurveyManager(_context);
            IAddSurveyResult result = manager.TryAddSurvey(title, description, content, dateLimit, responsesLimit, User.Identity.GetUserId());
            if (result.Result == Result.Success)
            {
                return Json(new { result = Result.Success.ToString(), url = Url.Action("Get", "Survey", new { id = result.Id, url = result.Url }) });
            }

            return Json(new { result = result.Result.ToString() });

        }

        [HttpGet]
        public ActionResult Get(int id = 1)
        {
            IGetSurveyManager manager = new GetSurveyManager(_context);
            var result = manager.GetSurvey(id);

            switch (result.Result)
            {
                case Result.Success:
                    {
                        ViewBag.Id = id;
                        return View(result.Content);
                    }
                case Result.NotFound:
                    return View("NotFound");
                case Result.EndDateReached:
                    return View("EndDateReached");
                case Result.ResponsesLimitReached:
                    return View("ResponsesLimitReached");
                default:
                    return View("Error");
            }
        }

    }
}