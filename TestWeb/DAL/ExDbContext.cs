﻿using System.Data.Entity;
using System.IO;
using Microsoft.AspNet.Identity.EntityFramework;
using TestWeb.Models;

namespace TestWeb.DAL
{
    public class ExDbContext : IdentityDbContext<ExUser>
    {
        public DbSet<Questionnaire> Questionnaires { get; set; }

        public DbSet<Question> Questions { get; set; }

        public DbSet<Answer> Answers { get; set; } 

        public DbSet<Response> Responses { get; set; }

        public DbSet<ResponseValue> ResponseValues { get; set; } 

        public ExDbContext()
            : base("DefaultConnection")
        {

        }

        static ExDbContext()
        {
            Database.SetInitializer(new ExDbInitializer());
        }

        public static ExDbContext Create()
        {
            return new ExDbContext();
        }

    }
}