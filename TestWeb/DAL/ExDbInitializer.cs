﻿using System.Data.Entity;
using System.IO;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using TestWeb.Models;

namespace TestWeb.DAL
{
    public class ExDbInitializer : DropCreateDatabaseIfModelChanges<ExDbContext>
    {
        protected override void Seed(ExDbContext context)
        {
            InitializeIdentity(context);
        }

        private void InitializeIdentity(ExDbContext context)
        {


            var userManager = HttpContext.Current.GetOwinContext().GetUserManager<ExUserManager>();
            var roleManager = HttpContext.Current.GetOwinContext().Get<ExRoleManager>();

            const string name = "admin@admin.pl";
            const string password = "Konik21!";
            const string adminRole = "Admin";

            var role = roleManager.FindById(adminRole);
            if (role == null)
            {
                roleManager.Create(new IdentityRole(adminRole));
            }

            var user = userManager.FindByName(name);
            if (user == null)
            {
                user = new ExUser
                {
                    UserName = name,
                    Email = name,
                    Whatever = "test",
                };
                userManager.Create(user, password);
            }

            var userRoles = userManager.GetRoles(user.Id);
            if (!userRoles.Contains(adminRole))
            {
                userManager.AddToRole(user.Id, adminRole);
            }
        }
    }
}