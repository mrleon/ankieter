﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestWeb.Infrastructure.Common.CSV
{
    public class CsvWriter : ICsvWriter
    {
        private readonly StringBuilder _content = new StringBuilder(); 

        public string GetCsv()
        {
            return _content.ToString();
        }


        public void WriteRow(IList<string> row)
        {
            var firstColumn = true;
            foreach (var value in row)
            {
                // Add separator if this isn't the first value
                if (!firstColumn)
                    _content.Append(';');
                // Implement special handling for values that contain comma or quote
                // Enclose in quotes and double up any double quotes
                if (value.IndexOfAny(new char[] { '"', ';' }) != -1)
                    _content.AppendFormat("\"{0}\"", value.Replace("\"", "\"\""));
                else
                    _content.Append(value);
                firstColumn = false;
            }
            _content.Append("\n");
        }
    }
}