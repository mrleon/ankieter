﻿using System.Collections.Generic;

namespace TestWeb.Infrastructure.Common.CSV
{
    public interface ICsvWriter
    {
        void WriteRow(IList<string> row);
        string GetCsv();
    }
}
