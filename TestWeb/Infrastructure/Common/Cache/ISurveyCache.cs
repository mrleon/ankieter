﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using TestWeb.Models;

namespace TestWeb.Infrastructure.Common.Cache
{
    public interface ISurveyCache
    {
        IDictionary<Question, IList<Answer>> GetQuestionnaireSet();

        Questionnaire GetQuestionnaire();

        JObject GetJson();
    }
}
