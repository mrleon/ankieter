﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Web;
using Newtonsoft.Json.Linq;
using TestWeb.Models;

namespace TestWeb.Infrastructure.Common.Cache
{
    public class SurveyCache : ISurveyCache
    {
        private readonly Questionnaire _questionnaire;
        private readonly IDictionary<Question, IList<Answer>> _dictionary;
        private readonly JObject _json;


        public SurveyCache(Questionnaire questionnaire, IDictionary<Question, IList<Answer>> dictionary, JObject json)
        {
            _questionnaire = questionnaire;
            _dictionary = dictionary;
            _json = json;
        }

        public IDictionary<Question, IList<Answer>> GetQuestionnaireSet()
        {
            return _dictionary;
        }

        public Questionnaire GetQuestionnaire()
        {
            return _questionnaire;
        }

        public JObject GetJson()
        {
            return _json;
        }
    }
}