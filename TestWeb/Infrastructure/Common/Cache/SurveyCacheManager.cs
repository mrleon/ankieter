﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Web;
using TestWeb.Models;

namespace TestWeb.Infrastructure.Common.Cache
{
    public sealed class SurveyCacheManager
    {
        static readonly SurveyCacheManager _instance = new SurveyCacheManager();

        private static readonly ObjectCache _cache = MemoryCache.Default;

        public static SurveyCacheManager Instance
        {
            get { return _instance; }
        }

        public ISurveyCache Get(int key)
        {
            return (ISurveyCache)_cache.Get(key.ToString());
        }

        public void Add(int key, ISurveyCache value)
        {
            _cache.Set(key.ToString(), value, new DateTimeOffset(new DateTime(2100, 1, 1)));
        }

    }
}