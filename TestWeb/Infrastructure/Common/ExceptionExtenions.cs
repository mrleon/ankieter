﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace TestWeb.Infrastructure.Common
{
    public static class ExceptionExtenions
    {
        /// <summary>
        /// Writes Exception Message and StackTrace to Debug
        /// </summary>
        /// <param name="exception"></param>
        public static void DebugWriteLine(this Exception exception)
        {
            Debug.WriteLine(exception.Message);
            Debug.WriteLine("STACKTRACE:");
            Debug.WriteLine(exception.StackTrace);
        }
    }
}