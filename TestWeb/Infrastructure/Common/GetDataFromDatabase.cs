﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using TestWeb.DAL;
using TestWeb.Models;

namespace TestWeb.Infrastructure.Common
{
    public class GetDataFromDatabase : IGetDataFromDatabase
    {
        public bool GetSurveySet(int questionnaireId, ExDbContext context, out Questionnaire questionnaire, out IDictionary<Question, IList<Answer>> dictionary)
        {
            var dict = new Dictionary<Question, IList<Answer>>(); // cannot use out ref parameter inside anonymous method
            //questionnaire = null;

            try
            {
                questionnaire = context.Questionnaires.FirstOrDefault(t => t.ID == questionnaireId);
                if (questionnaire == null)
                {
                    throw new Exception("QUESTIONNAIRE NOT FOUND");
                }

                var questions = questionnaire.Questions.ToList();
                questions.ForEach(t => dict.Add(t, new List<Answer>()));

                var qustionsIds = questions.Select(t => t.ID).ToList();
                var answers = context.Answers.Where(t => qustionsIds.Contains(t.QuestionID)).ToList();
                answers.ForEach(t => dict.First(s => s.Key.ID == t.QuestionID).Value.Add(t));

                Debug.WriteLine("test");

            }
            catch (Exception e)
            {
                e.DebugWriteLine();

                dictionary = null;
                questionnaire = null;

                return false;

            }

            dictionary = dict;
            return true;
        }

        public bool GetResponses(int questionnaireId, ExDbContext context, out Questionnaire questionnaire, out IList<Question> questions, out IList<ResponseValue> responseValues)
        {
            try
            {
                questionnaire = context.Questionnaires.First(t => t.ID == questionnaireId);

                questions = context.Questions.Where(t => t.QuestionnaireID == questionnaireId).ToList();

                var addedResponsesIds =
                    context.Responses.Where(t => t.QuestionnaireID == questionnaireId).Select(t => t.ID).ToList();

                responseValues = context.ResponseValues.Where(t => addedResponsesIds.Contains(t.ResponseID)).ToList();

            }
            catch (Exception e)
            {
                e.DebugWriteLine();

                questionnaire = null;
                questions = null;
                responseValues = null;
                return false;
            }

            return true;
        }
    }
}