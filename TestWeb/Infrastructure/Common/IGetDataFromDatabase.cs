﻿using System.Collections.Generic;
using TestWeb.DAL;
using TestWeb.Models;

namespace TestWeb.Infrastructure.Common
{
    public interface IGetDataFromDatabase
    {
        bool GetSurveySet(int questionnaireId, ExDbContext context, out Questionnaire questionnaire, out IDictionary<Question, IList<Answer>> dictionary);

        bool GetResponses(int questionnaireId, ExDbContext context, out Questionnaire questionnaire, out IList<Question> questions, out IList<ResponseValue> responseValues);

    }
}