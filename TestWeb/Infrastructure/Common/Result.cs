﻿namespace TestWeb.Infrastructure.Common
{
    public enum Result
    {
        Success,
        ParserError,
        ValidatorError,
        DatabaseError,
        EndDateReached,
        ResponsesLimitReached,
        NotFound,
        Error
    }
}