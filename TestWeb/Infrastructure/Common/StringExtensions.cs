﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using Microsoft.Ajax.Utilities;

namespace TestWeb.Infrastructure.Common
{
    public static class StringExtensions
    {
        public static string GetUrlFromTitle(this string title)
        {
            IList<char> charsToReplace = "~`!@#$%^&*()-_=+ []{}|;:\"\\',.<>/?".ToList();

            StringBuilder newString = new StringBuilder(title);

            charsToReplace.ForEach(t => newString.Replace(t, '-'));

            return newString.ToString();
        }

        public static bool IsOptionLongEnough(this string option)
        {
            return option.Length > 3;
        }

    }
}