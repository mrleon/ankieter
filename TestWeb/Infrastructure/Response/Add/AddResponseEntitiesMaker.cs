﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TestWeb.Models;
using WebGrease.Css.Extensions;

namespace TestWeb.Infrastructure.Response.Add
{
    public class AddResponseEntitiesMaker : IAddResponseEntitiesMaker
    {
        public bool MakeEntities(Questionnaire questionnaire, IDictionary<Question, IList<Answer>> dictionary, out Models.Response response, out IList<ResponseValue> responsesValues)
        {
            response = new Models.Response { Created = DateTime.Now, QuestionnaireID = questionnaire.ID, ResponseValue = new List<ResponseValue>()};

            responsesValues = (from pair in dictionary from answer in pair.Value select new ResponseValue {QuestionIndex = pair.Key.Index, Value = answer.Content}).ToList();

            return true;
        }
    }
}