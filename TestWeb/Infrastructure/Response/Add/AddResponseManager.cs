﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TestWeb.DAL;
using TestWeb.Infrastructure.Common;
using TestWeb.Infrastructure.Common.Cache;
using TestWeb.Models;

namespace TestWeb.Infrastructure.Response.Add
{
    public class AddResponseManager : IAddResponseManager
    {
        private readonly ExDbContext _context;

        public IAddResponseResult TryAddResponse(int questionnaireId, string json)
        {
            Questionnaire questionnaire = _context.Questionnaires.FirstOrDefault(t => t.ID == questionnaireId);

            if (questionnaire == null)
            {
                return new AddResponseResult(Result.NotFound);
            }

            if (IsQuestionnaireClosed(questionnaire))
            {
                return new AddResponseResult(Result.EndDateReached);
            }
            if (IsQuestionnaireResponsesLimitReached(questionnaire))
            {
                return new AddResponseResult(Result.ResponsesLimitReached);
            }

            IDictionary<Question, IList<Answer>> parsedDictionary;
            IDictionary<Question, IList<Answer>> pairs = new Dictionary<Question, IList<Answer>>();



            IAddResponseParser parser = new AddResponseParser();
            if (!parser.TryParseResponse(json, out parsedDictionary))
            {
                return new AddResponseResult(Result.ParserError);
            }

            var cacheManager = SurveyCacheManager.Instance;
            var dbDictionary = cacheManager.Get(questionnaireId);
            if (dbDictionary == null)
            {
                IGetDataFromDatabase dataFromDatabase = new GetDataFromDatabase();
                if (!dataFromDatabase.GetSurveySet(questionnaireId, _context, out questionnaire, out pairs))
                {
                    return new AddResponseResult(Result.DatabaseError);
                }
            }
            else
            {
                pairs = new Dictionary<Question, IList<Answer>>(dbDictionary.GetQuestionnaireSet());
            }

            IAddResponseValidator validator = new AddResponseValidator();
            if (!validator.TryValidateResponse(pairs, parsedDictionary))
            {
                return new AddResponseResult(Result.ValidatorError);
            }

            Models.Response response;
            IList<ResponseValue> responseValues;

            IAddResponseEntitiesMaker maker = new AddResponseEntitiesMaker();
            if (!maker.MakeEntities(questionnaire, parsedDictionary, out response, out responseValues))
            {
                return new AddResponseResult(Result.Error);
            }

            IAddResponseToDatabase addResponseToDatabase = new AddResponseToDatabase();
            if (!addResponseToDatabase.AddResponse(response, responseValues, _context))
            {
                return new AddResponseResult(Result.DatabaseError);
            }

            return new AddResponseResult(Result.Success);
        }

        public AddResponseManager(ExDbContext context)
        {
            _context = context;
        }

        private static bool IsQuestionnaireClosed(Questionnaire questionnaire)
        {
            return questionnaire.LimitDateTime.HasValue && DateTime.Now < questionnaire.LimitDateTime.Value;
        }

        private static bool IsQuestionnaireResponsesLimitReached(Questionnaire questionnaire)
        {
            return questionnaire.ResponsesLimit.HasValue && questionnaire.Responses.Count >= questionnaire.ResponsesLimit;
        }
    }
}