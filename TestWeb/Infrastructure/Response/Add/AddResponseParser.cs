﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using Microsoft.Ajax.Utilities;
using Newtonsoft.Json.Linq;
using TestWeb.Infrastructure.Common;
using TestWeb.Models;

namespace TestWeb.Infrastructure.Response.Add
{
    public class AddResponseParser : IAddResponseParser
    {
        public bool TryParseResponse(string json, out IDictionary<Question, IList<Answer>> dictionary)
        {
            dictionary = new Dictionary<Question, IList<Answer>>();

            dynamic parsed = JArray.Parse(json);

            try
            {
                foreach (var question in parsed)
                {
                    var parsedQuestion = new Question();
                    parsedQuestion.Index = (int)question.index.Value;
                    parsedQuestion.QuestionType = Enum.Parse(typeof(QuestionType), question.type.Value);

                    var answerList = new List<Answer>();
                    if (IsRadioOrCheckbox(parsedQuestion.QuestionType))
                    {
                        foreach (var answer in question.options)
                        {
                            if ((bool)answer.selected)
                            {
                                answerList.Add(new Answer { Content = answer.content.Value });
                            }
                        }

                    }
                    else if (parsedQuestion.QuestionType == QuestionType.Text)
                    {
                        foreach (var answer in question.options)
                        {
                            if (!String.IsNullOrWhiteSpace(answer.Value))
                            {
                                answerList.Add(new Answer { Content = answer.Value });
                            }
                        }
                    }
                    else
                    {
                        if (!String.IsNullOrWhiteSpace(question.content.Value))
                        {
                            answerList.Add(new Answer { Content = question.content.Value });
                        }
                    }

                    dictionary.Add(parsedQuestion, answerList);

                }
            }
            catch (Exception e)
            {
                e.DebugWriteLine();

                dictionary = null;
                return false;
            }


            dictionary = dictionary.OrderBy(t => t.Key.Index).ToDictionary(k => k.Key, v => v.Value);
            return true;
        }

        private static bool IsRadioOrCheckbox(QuestionType type)
        {
            return type == QuestionType.Checkbox || type == QuestionType.Radio;
        }
    }
}