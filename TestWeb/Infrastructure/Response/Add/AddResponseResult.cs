﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TestWeb.Infrastructure.Common;

namespace TestWeb.Infrastructure.Response.Add
{
    public class AddResponseResult : IAddResponseResult
    {
        private readonly Result _result;

        public Result Result()
        {
            return _result;
        }

        public AddResponseResult(Result result)
        {
            _result = result;
        }
    }
}