﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TestWeb.DAL;
using TestWeb.Infrastructure.Common;
using TestWeb.Models;

namespace TestWeb.Infrastructure.Response.Add
{
    public class AddResponseToDatabase : IAddResponseToDatabase
    {
        public bool AddResponse(Models.Response response, IList<ResponseValue> responseValues, ExDbContext context)
        {
            try
            {
                response.ResponseValue = new List<ResponseValue>();
                foreach (var value in responseValues)
                {
                    response.ResponseValue.Add(value);
                }

                context.Responses.Add(response);
                context.SaveChanges();
            }
            catch (Exception e)
            {
                e.DebugWriteLine();

                return false;
            }

            return true;
        }
    }
}