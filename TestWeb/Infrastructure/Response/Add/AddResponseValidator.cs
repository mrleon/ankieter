﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TestWeb.Infrastructure.Common;
using TestWeb.Models;

namespace TestWeb.Infrastructure.Response.Add
{
    public class AddResponseValidator : IAddResponseValidator
    {
        public bool TryValidateResponse(IDictionary<Question, IList<Answer>> dbDictionary, IDictionary<Question, IList<Answer>> parsedDictionary)
        {
            var validator = new QuestionValidator();

            var tmpDictionary = new Dictionary<Question, IList<Answer>>(parsedDictionary);

            try
            {
                foreach (var pair in dbDictionary)
                {
                    var t = tmpDictionary.FirstOrDefault(s => s.Key.Index == pair.Key.Index);
                    // if null
                    if (t.Equals(default(KeyValuePair<Question, IList<Answer>>)) || !validator.TryValidateQuestion(pair, t))
                    {
                        return false;
                    }
                    tmpDictionary.Remove(t.Key);
                }
            }
            catch (Exception e)
            {
                e.DebugWriteLine();

                return false;
            }

            return tmpDictionary.Count == 0;
        }
    }
}