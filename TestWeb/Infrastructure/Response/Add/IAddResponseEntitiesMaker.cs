﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TestWeb.Models;

namespace TestWeb.Infrastructure.Response.Add
{
    public interface IAddResponseEntitiesMaker
    {
        bool MakeEntities(Questionnaire questionnaire, IDictionary<Question, IList<Answer>> dictionary, out Models.Response response, out IList<ResponseValue> responsesValues);
    }
}
