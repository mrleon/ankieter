﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestWeb.Models;

namespace TestWeb.Infrastructure.Response.Add
{
    public interface IAddResponseParser
    {
        bool TryParseResponse(string json, out IDictionary<Question, IList<Answer>> dictionary);
    }
}
