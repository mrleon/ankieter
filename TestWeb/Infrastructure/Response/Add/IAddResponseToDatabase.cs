﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestWeb.DAL;
using TestWeb.Models;

namespace TestWeb.Infrastructure.Response.Add
{
    public interface IAddResponseToDatabase
    {
        bool AddResponse(Models.Response response, IList<ResponseValue> responseValues, ExDbContext context);
    }
}
