﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestWeb.Models;

namespace TestWeb.Infrastructure.Response.Add
{
    public interface IAddResponseValidator
    {
        bool TryValidateResponse(IDictionary<Question, IList<Answer>>  dictionary, IDictionary<Question, IList<Answer>> parsedDictionary);
    }
}
