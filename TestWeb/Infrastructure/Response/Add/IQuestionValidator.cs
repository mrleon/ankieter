﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestWeb.Models;

namespace TestWeb.Infrastructure.Response.Add
{
    public interface IQuestionValidator
    {
        bool TryValidateQuestion(KeyValuePair<Question, IList<Answer>> dbQuestion, KeyValuePair<Question, IList<Answer>> parsedQuestion);
    }
}
