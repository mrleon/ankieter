﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TestWeb.Models;
using WebGrease.Css.Extensions;

namespace TestWeb.Infrastructure.Response.Add
{
    public class QuestionValidator : IQuestionValidator
    {
        public bool TryValidateQuestion(KeyValuePair<Question, IList<Answer>> dbQuestion, KeyValuePair<Question, IList<Answer>> parsedQuestion)
        {
            if (!HasTheSameType(dbQuestion.Key, parsedQuestion.Key))
            {
                return false;
            }

            var answerList = new List<string>(parsedQuestion.Value.Select(t => t.Content));

            switch (dbQuestion.Key.QuestionType)
            {
                case QuestionType.Multiline:
                    {
                        var answer = answerList.FirstOrDefault();
                        if (dbQuestion.Key.Required && !IsMultilineAnswerCorrect(answer))
                        {
                            return false;
                        }
                        break;
                    }
                case QuestionType.Text:
                    {
                        if (dbQuestion.Key.Required && !AreTextAnswersCorrect(answerList))
                        {
                            return false;
                        }
                        break;
                    }
                case QuestionType.Radio:
                    {
                        var answer = answerList.FirstOrDefault();
                        if (dbQuestion.Key.Required && answer == null)
                        {
                            return false;
                        }
                        if (!IsRadioAnswerCorrect(dbQuestion.Value, answer) || answerList.Count != 1)
                        {
                            return false;
                        }
                        break;
                    }
                case QuestionType.Checkbox:
                    {
                        var answersAreCorrect = AreCheckboxAnswersCorrect(dbQuestion.Value, new List<string>(answerList));
                        if (!answersAreCorrect || (dbQuestion.Key.Required && answerList.Count == 0))
                        {
                            return false;
                        }
                        break;
                    }
            }

            return true;
        }

        private static bool HasTheSameType(Question question, Question parsedQuestion)
        {
            return question.QuestionType == parsedQuestion.QuestionType;
        }

        private static bool IsMultilineAnswerCorrect(string content)
        {
            const int minimumLength = 1;
            const int maximumLength = 1000;

            var length = content.Length;

            return !String.IsNullOrWhiteSpace(content) && length >= minimumLength && length <= maximumLength;
        }

        private static bool AreTextAnswersCorrect(IEnumerable<string> answers)
        {
            // Any answer is accecptable
            return answers.Any(answer => !String.IsNullOrWhiteSpace(answer));
        }

        private static bool IsRadioAnswerCorrect(IEnumerable<Answer> answers, string parsedAnswer)
        {
            // parsedAnswer is real answer
            return answers.Any(t => t.Content == parsedAnswer);
        }

        private static bool AreCheckboxAnswersCorrect(IEnumerable<Answer> answers, IList<string> parsedAnswer)
        {
            // parsedAnswer contains ONLY real answers and nothing more
            answers.ForEach(t => parsedAnswer.Remove(t.Content));

            return parsedAnswer.Count == 0;
        }
    }
}