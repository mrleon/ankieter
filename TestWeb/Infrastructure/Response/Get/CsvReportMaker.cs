﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using TestWeb.Models;
using TestWeb.Models.ResponsesModels;
using TestWeb.ViewModels;

namespace TestWeb.Infrastructure.Response.Get
{
    public class CsvReportMaker : ICsvReportMaker
    {
        private const char Separator = ',';

        public IReport MakeCsvReport(Questionnaire questionnaire, IList<Question> questions, IList<QuestionResponses> questionResponseses)
        {
            var target = new StringBuilder();
            target.AppendFormat("Tytuł ankiety{0}{1}{0}\n", Separator, questionnaire.Title);
            target.AppendFormat("Opis ankiety{0}{1}{0}\n", Separator, questionnaire.Description);
            target.AppendFormat("Liczba odpowiedzi{0}{1}{0}\n", Separator, questionnaire.Responses.Count);

            foreach (var question in questions.OrderBy(t => t.Index))
            {
                target.AppendLine();
                target.AppendFormat("Pytanie {0}{1}{2}{1}\n", question.Index + 1, Separator, question.Query);
                target.AppendFormat("Wymagane{1}{0}{1}\n", question.Required ? "Tak" : "Nie", Separator);
                target.AppendLine();

                var answers = new Dictionary<string, int>();
                var indexesList = questionResponseses.FirstOrDefault(t => t.Index == question.Index);
                if (indexesList != null)
                {
                    answers = indexesList.Dictionary.OrderByDescending(t => t.Value)
                        .ToDictionary(k => k.Key, v => v.Value);
                }

                target.AppendFormat("Odpowiedź{0}Ilość{0}\n", Separator);
                foreach (var answer in answers)
                {
                    target.AppendFormat("{1}{0}{2}{0}\n", Separator, answer.Key, answer.Value);
                }
            }
            return new Report(target.ToString(), GetContentType(), GetFileName(questionnaire));
        }

        private static string GetFileName(Questionnaire questionnaire)
        {
            if (questionnaire == null)
            {
                return null;
            }

            var culture = new CultureInfo("pl-PL");

            return String.Format("Raport z ankiety: {0} - {1}.csv", questionnaire.Title, DateTime.Now.ToString("HH:mm d MMMM yyyy", culture));
        }

        private static string GetContentType()
        {
            return "text/csv";
        }
    }
}