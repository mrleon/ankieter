﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TestWeb.DAL;
using TestWeb.Infrastructure.Common;
using TestWeb.Models;
using TestWeb.ViewModels;

namespace TestWeb.Infrastructure.Response.Get
{
    public class GetResponsesManager : IGetResponsesManager
    {
        private readonly ExDbContext _context;

        public GetResponsesManager(ExDbContext context)
        {
            _context = context;
        }

        public IReport GetCsvReport(int questionnaireId)
        {
            Questionnaire questionnaire;
            IList<Question> questions;
            IList<ResponseValue> responseValues;

            IGetDataFromDatabase dataFromDatabase = new GetDataFromDatabase();

            if (!dataFromDatabase.GetResponses(questionnaireId, _context, out questionnaire, out questions, out responseValues))
            {
                return null;
            }

            IQuestionResponsesMaker maker = new QuestionResponsesMaker();
            var questionResponses = maker.MakeQuestionResponses(questions, responseValues);


            ICsvReportMaker reportMaker = new CsvReportMaker();
            var report = reportMaker.MakeCsvReport(questionnaire, questions, questionResponses);

            return report;
        }
        public SurveyResponsesViewModel GetSurveyResponses(int questionnaireId)
        {
            Questionnaire questionnaire;
            IList<Question> questions;
            IList<ResponseValue> responseValues;

            IGetDataFromDatabase dataFromDatabase = new GetDataFromDatabase();

            if (!dataFromDatabase.GetResponses(questionnaireId, _context, out questionnaire, out questions, out responseValues))
            {
                return null;
            }

            IQuestionResponsesMaker maker = new QuestionResponsesMaker();
            var questionResponses = maker.MakeQuestionResponses(questions, responseValues);

            IMakeSurveyResponsesViewModel makeModel = new MakeSurveyResponsesViewModel();
            var model = makeModel.Make(questionnaire, questions, questionResponses);

            return model;
        }
    }
}