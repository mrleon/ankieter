﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestWeb.ViewModels;

namespace TestWeb.Infrastructure.Response.Get
{
    public interface IGetResponsesManager
    {
        IReport GetCsvReport(int questionnaireId);
        SurveyResponsesViewModel GetSurveyResponses(int questionnaireId);
    }
}
