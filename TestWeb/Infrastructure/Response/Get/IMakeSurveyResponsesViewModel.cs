﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestWeb.Models;
using TestWeb.Models.ResponsesModels;
using TestWeb.ViewModels;

namespace TestWeb.Infrastructure.Response.Get
{
    public interface IMakeSurveyResponsesViewModel
    {
        SurveyResponsesViewModel Make(Questionnaire questionnaire, IList<Question> questions,
            IList<QuestionResponses> questionResponseses);
    }
}
