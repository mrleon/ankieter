﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestWeb.Models;
using TestWeb.Models.ResponsesModels;

namespace TestWeb.Infrastructure.Response.Get
{
    public interface IQuestionResponsesMaker
    {
        IList<QuestionResponses> MakeQuestionResponses(IList<Question> questionsIList,
            IList<ResponseValue> responseValues);
    }
}
