﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace TestWeb.Infrastructure.Response.Get
{
    public interface IReport
    {
        string Content { get; set; }
        string ContentType { get; set; }
        string FileName { get; set; }
    }
}
