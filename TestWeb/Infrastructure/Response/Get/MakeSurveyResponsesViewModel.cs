﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TestWeb.Models;
using TestWeb.Models.ResponsesModels;
using TestWeb.ViewModels;

namespace TestWeb.Infrastructure.Response.Get
{
    public class MakeSurveyResponsesViewModel : IMakeSurveyResponsesViewModel
    {
        public SurveyResponsesViewModel Make(Questionnaire questionnaire, IList<Question> questions, IList<QuestionResponses> questionResponseses)
        {
            var model = new SurveyResponsesViewModel
            {
                Title = questionnaire.Title,
                Description = questionnaire.Description,
                ResponsesCount = questionnaire.Responses.Count,
                Questions = new List<QuestionResponsesViewModel>()
            };

            foreach (var question in questions)
            {
                var questionViewModel = new QuestionResponsesViewModel
                {
                    Title = question.Query,
                    QuestionTypeDescription = GetDescriptionOfType(question.QuestionType),
                    Index = question.Index,
                    IsRequired = question.Required,
                    Answers = new Dictionary<string, int>()
                };

                var tmp = questionResponseses.FirstOrDefault(t => t.Index == question.Index);
                if (tmp != null)
                {
                    questionViewModel.Answers = tmp.Dictionary.OrderByDescending(t => t.Value)
                        .ToDictionary(k => k.Key, v => v.Value);
                }

                model.Questions.Add(questionViewModel);
            }

            model.Questions = model.Questions.OrderBy(t => t.Index).ToList();

            return model;
        }

        private static string GetDescriptionOfType(QuestionType type)
        {
            switch (type)
            {
                case QuestionType.Radio:
                    return "Pytanie jednokrotnego wyboru";
                case QuestionType.Checkbox:
                    return "Pytanie wielokrotnego wyboru";
                case QuestionType.Multiline:
                    return "Pytanie opisowe";
                case QuestionType.Text:
                    return "Pytanie tekstowe";
            }

            return String.Empty;
        }
    }
}