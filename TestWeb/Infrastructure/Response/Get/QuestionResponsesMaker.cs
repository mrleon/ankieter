﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TestWeb.Models;
using TestWeb.Models.ResponsesModels;
using WebGrease.Css.Extensions;

namespace TestWeb.Infrastructure.Response.Get
{
    public class QuestionResponsesMaker : IQuestionResponsesMaker
    {
        public IList<QuestionResponses> MakeQuestionResponses(IList<Question> questionsIList, IList<ResponseValue> responseValues)
        {
            var result = new List<QuestionResponses>();

            foreach (var question in questionsIList)
            {
                //var dictionary = new Dictionary<string, int>();
                var dictionary = question.Answers.ToDictionary(t => t.Content, s => 0);
                var question1 = question;
                var properIndexList = responseValues.Where(t => t.QuestionIndex == question1.Index);
                foreach (var response in properIndexList)
                {
                    if (dictionary.ContainsKey(response.Value))
                    {
                        dictionary[response.Value]++;
                    }
                    else
                    {
                        dictionary.Add(response.Value, 1);
                    }
                }
                result.Add(new QuestionResponses(question.Query, question.Index, dictionary));
            }
            return result;
        }
    }
}