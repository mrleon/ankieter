﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestWeb.Infrastructure.Response.Get
{
    public class Report : IReport
    {
        public string Content { get; set; }
        public string ContentType { get; set; }
        public string FileName { get; set; }

        public Report(string content, string contentType, string fileName)
        {
            Content = content;
            ContentType = contentType;
            FileName = fileName;
        }
    }
}