﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using TestWeb.DAL;
using TestWeb.Infrastructure.Common;
using TestWeb.Models;
using WebGrease.Css.Extensions;

namespace TestWeb.Infrastructure.Survey.Add
{
    public class AddSurveyManager : IAddSurveyManager
    {
        private readonly ExDbContext _context;

        public AddSurveyManager(ExDbContext context)
        {
            _context = context;
        }

        public IAddSurveyResult TryAddSurvey(string title, string description, string content, string dateLimit, int? responsesLimit, string userId)
        {
            IAddSurveyParser parser = new AddSurveyParser();

            IDictionary<Question, IList<Answer>> dictionary;
            if (!parser.TryParseSurvey(content, out dictionary))
            {
                return new AddSurveyResult(Result.ParserError, String.Empty, null);
            }
            var date = parser.TryParseDateTimeLimit(dateLimit);

            IAddSurveyValidator validator = new AddSurveyValidator();

            if (!validator.TryValidateSurvey(title, description, date, responsesLimit, dictionary))
            {
                return new AddSurveyResult(Result.ValidatorError, String.Empty, null);
            }

            IAddSurveyToDatabase addSurveyToDatabase = new AddSurveyToDatabase();
            var questionnaire = addSurveyToDatabase.TryAddSurveyToDatabase(title, description, date, responsesLimit, dictionary, userId, _context);

            if (questionnaire == null)
            {
                return new AddSurveyResult(Result.DatabaseError, String.Empty, null);
            }

            return new AddSurveyResult(Result.Success, questionnaire.Url, questionnaire.ID);

        }

    }
}