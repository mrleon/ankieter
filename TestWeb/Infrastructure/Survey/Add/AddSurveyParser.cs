﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Newtonsoft.Json.Linq;
using TestWeb.Models;

namespace TestWeb.Infrastructure.Survey.Add
{
    public class AddSurveyParser : IAddSurveyParser
    {
        public bool TryParseSurvey(string content, out IDictionary<Question, IList<Answer>> pairs)
        {
            dynamic json = JArray.Parse(content);
            pairs = new Dictionary<Question, IList<Answer>>();


            try
            {
                foreach (var question in json)
                {
                    var newQuestion = new Question();
                    pairs.Add(newQuestion, null);


                    newQuestion.Query = question.title.Value;
                    newQuestion.Description = question.description.Value;
                    newQuestion.Required = question.required.Value;
                    newQuestion.QuestionType = Enum.Parse(typeof(QuestionType), question.type.Value);
                    newQuestion.Index = (int)question.index.Value;

                    if (HasOptions(newQuestion.QuestionType))
                    {
                        IList<Answer> answers = new List<Answer>();
                        var answerIndex = 0;
                        foreach (var answer in question.options)
                        {
                            var ans = new Answer { Content = answer.Value, Index = answerIndex };

                            answers.Add(ans);

                            answerIndex++;
                        }

                        pairs[newQuestion] = answers;
                    }
                    else if (newQuestion.QuestionType == QuestionType.Text)
                    {
                        newQuestion.AnswerNumber = question.options.Count;
                    }
                }
            }
            catch (Exception e)
            {
                pairs = null;
                Debug.WriteLine(e.Message);
                Debug.WriteLine(e.StackTrace);
                return false;
            }

            return true;
        }

        private static bool HasOptions(QuestionType type)
        {
            return type == QuestionType.Checkbox || type == QuestionType.Radio;
        }


        public DateTime? TryParseDateTimeLimit(string date)
        {
            DateTime parsed;
            if (DateTime.TryParse(date, out parsed))
            {
                return parsed;
            }

            return null;
        }
    }
}