﻿
using TestWeb.Infrastructure.Common;

namespace TestWeb.Infrastructure.Survey.Add
{
    public class AddSurveyResult : IAddSurveyResult
    {
        public Result Result { get; set; }

        public string Url { get; set; }
        public int? Id { get; set; }

        public AddSurveyResult(Result result, string url, int? id)
        {
            Result = result;
            Url = url;
            Id = id;
        }
    }
}