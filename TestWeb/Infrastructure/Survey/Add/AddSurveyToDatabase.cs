﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using TestWeb.DAL;
using TestWeb.Infrastructure.Common;
using TestWeb.Models;
using WebGrease.Css.Extensions;

namespace TestWeb.Infrastructure.Survey.Add
{
    public class AddSurveyToDatabase : IAddSurveyToDatabase
    {
        public Questionnaire TryAddSurveyToDatabase(string title, string description, DateTime? dateLimit, int? responsesLimit, IDictionary<Question, IList<Answer>> pairs, string userId, ExDbContext context)
        {
            var questionnaire = new Questionnaire()
            {
                Title = title,
                Description = description,
                ExUserID = userId,
                Created = DateTime.Now,
                Url = title.GetUrlFromTitle(),
                LimitDateTime = dateLimit,
                ResponsesLimit = responsesLimit == 0 ? null : responsesLimit,
            };

            foreach (var pair in pairs.Where(pair => pair.Value != null))
            {
                var pair1 = pair;
                pair.Value.ForEach(t => pair1.Key.Answers = new List<Answer>());
                pair.Value.ForEach(t => pair1.Key.Answers.Add(t));
            }

            questionnaire.Questions = new List<Question>();
            pairs.ForEach(t => questionnaire.Questions.Add(t.Key));


            context.Questionnaires.Add(questionnaire);
            try
            {
                context.SaveChanges();
            }
            catch (Exception e)
            {
                e.DebugWriteLine();
                return null;
            }

            return questionnaire;
        }

    }
}