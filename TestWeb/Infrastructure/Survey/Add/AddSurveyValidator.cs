﻿using System;
using System.Collections.Generic;
using System.Linq;
using TestWeb.Models;

namespace TestWeb.Infrastructure.Survey.Add
{
    public class AddSurveyValidator : IAddSurveyValidator
    {
        public bool TryValidateSurvey(string title, string description, DateTime? dateLimit, int? responsesLimit, IDictionary<Question, IList<Answer>> pairs)
        {
            if (pairs == null)
            {
                return false;
            }
            if (!ValidateTitle(title) || !ValidateDescription(description) || !ValidateNumberOfQuestions(pairs.Count))
            {
                return false;
            }
            
            if (!IsAnyQuestionRequired(pairs.Keys))
            {
                return false;
            }

            IQuestionValidator questionValidator = new QuestionValidator();
            return pairs.All(pair => questionValidator.TryValidateQuestion(pair.Key, pair.Value));
        }

        private static bool ValidateTitle(string title)
        {
            const int minimumLength = 5;
            const int maximumLength = 100;

            //title = title.Trim();

            return !String.IsNullOrWhiteSpace(title) && title.Length >= minimumLength && title.Length <= maximumLength;

        }

        private static bool ValidateDescription(string description)
        {
            const int maximumLength = 300;

            return !String.IsNullOrWhiteSpace(description) && description.Length <= maximumLength;
        }

        private static bool ValidateNumberOfQuestions(int count)
        {
            const int minimumNumberOfQuestions = 1;
            const int maximumNumberOfQuestion = 100;

            return count >= minimumNumberOfQuestions && count <= maximumNumberOfQuestion;
        }

        private static bool IsAnyQuestionRequired(IEnumerable<Question> questions)
        {
            return questions.Count(t => t.Required) > 0;
        }

        private static bool ValidateDateLimit(DateTime? date)
        {
            const int minutesToAdd = 5;

            // date before actual date or has no value
            return !date.HasValue || DateTime.Now.AddMinutes(minutesToAdd) < date.Value;

        }

        private static bool ValidateResponsesLimit(int? limit)
        {
            return !limit.HasValue || limit.Value > 0 && limit.Value <= 10000;
        }
    }
}