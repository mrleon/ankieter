﻿using TestWeb.DAL;
using TestWeb.Models;

namespace TestWeb.Infrastructure.Survey.Add
{
    public interface IAddSurveyManager
    {
        IAddSurveyResult TryAddSurvey(string title, string description, string content, string dateLimit, int? responsesLimit, string userId);
    }
}
