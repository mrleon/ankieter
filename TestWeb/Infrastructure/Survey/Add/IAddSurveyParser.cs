﻿using System;
using System.Collections.Generic;
using TestWeb.Models;

namespace TestWeb.Infrastructure.Survey.Add
{
    interface IAddSurveyParser
    {
        bool TryParseSurvey(string content, out IDictionary<Question, IList<Answer>> pairs);
        DateTime? TryParseDateTimeLimit(string date);
    }
}
