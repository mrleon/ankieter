﻿using TestWeb.Infrastructure.Common;

namespace TestWeb.Infrastructure.Survey.Add
{
    public interface IAddSurveyResult
    {
        Result Result { get; }

        string Url { get; }

        int? Id { get; }
    }
}
