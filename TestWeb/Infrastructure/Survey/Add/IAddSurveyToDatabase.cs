﻿using System;
using System.Collections.Generic;
using TestWeb.DAL;
using TestWeb.Models;

namespace TestWeb.Infrastructure.Survey.Add
{
    interface IAddSurveyToDatabase
    {
        Questionnaire TryAddSurveyToDatabase(string title, string description, DateTime? dateLimit, int? responsesLimit, IDictionary<Question, IList<Answer>> pairs,
            string userId, ExDbContext context);
    }
}
