﻿using System;
using System.Collections.Generic;
using TestWeb.Models;

namespace TestWeb.Infrastructure.Survey.Add
{
    interface IAddSurveyValidator
    {
        bool TryValidateSurvey(string title, string description, DateTime? dateLimit, int? responsesLimit, IDictionary<Question, IList<Answer>> pairs);
    }
}
