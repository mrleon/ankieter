﻿using System.Collections.Generic;
using TestWeb.Models;

namespace TestWeb.Infrastructure.Survey.Add
{
    public interface IQuestionValidator
    {
        bool TryValidateQuestion(Question question, IList<Answer> answers);
    }
}