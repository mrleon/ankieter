﻿using System;
using System.Collections.Generic;
using System.Linq;
using TestWeb.Models;

namespace TestWeb.Infrastructure.Survey.Add
{
    public class QuestionValidator : IQuestionValidator
    {
        public bool TryValidateQuestion(Question question, IList<Answer> answers)
        {
            if (!ValidateTitle(question.Query) || !ValidateDescription(question.Description))
            {
                return false;
            }

            if (question.QuestionType == QuestionType.Text && question.AnswerNumber < 1)
            {
                return false; // must have at least one field
            }
            if (HasAnswers(question.QuestionType))
            {
                if (answers == null)
                {
                    return false;
                }
                if (!ValidateNumberOfAnswers(answers.Count, question.QuestionType))
                {
                    return false;
                }
                if (answers.Any(answer => !ValidateAnswer(answer.Content)))
                {
                    return false;
                }
            }
            return true;
        }

        private static bool ValidateTitle(string title)
        {
            const int minimumLength = 3;
            const int maximumLength = 100;

            //title = title.Trim();

            return !String.IsNullOrWhiteSpace(title) && title.Length >= minimumLength && title.Length <= maximumLength;

        }

        private static bool ValidateDescription(string description)
        {
            const int maximumLength = 300;

            return description == null || description.Length <= maximumLength;
        }

        private static bool ValidateNumberOfAnswers(int count, QuestionType questionType)
        {
            if (questionType == QuestionType.Checkbox)
            {
                return count >= 1;
            }
            else if (questionType == QuestionType.Radio)
            {
                return count >= 2;
            }
            else
            {
                return false;
            }
        }

        private static bool ValidateAnswer(string content)
        {
            const int minimumLength = 1;
            const int maximumLength = 100;

            content = content.Trim();

            return !String.IsNullOrWhiteSpace(content) && content.Length >= minimumLength &&
                   content.Length <= maximumLength;

        }

        private static bool HasAnswers(QuestionType type)
        {
            return type == QuestionType.Checkbox || type == QuestionType.Radio;
        }
    }
}