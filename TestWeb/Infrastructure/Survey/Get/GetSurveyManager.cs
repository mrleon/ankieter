﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json.Linq;
using TestWeb.DAL;
using TestWeb.Infrastructure.Common;
using TestWeb.Infrastructure.Common.Cache;
using TestWeb.Infrastructure.Survey.Add;
using TestWeb.Models;

namespace TestWeb.Infrastructure.Survey.Get
{
    public class GetSurveyManager : IGetSurveyManager
    {
        private readonly ExDbContext _context;
        private readonly SurveyCacheManager _cacheManager = SurveyCacheManager.Instance;

        public GetSurveyManager(ExDbContext context)
        {
            _context = context;
        }

        public IGetSurveyResult GetSurvey(int id)
        {
            JObject json;
            var questionnaire = _context.Questionnaires.FirstOrDefault(t => t.ID == id);
            IDictionary<Question, IList<Answer>> dictionary;

            if (questionnaire == null)
            {
                return new GetSurveyResult(Result.NotFound, null);
            }

            if (IsTimeLimitReached(questionnaire))
            {
                return new GetSurveyResult(Result.EndDateReached, null);
            }

            if (IsResponsesLimitReached(questionnaire))
            {
                return new GetSurveyResult(Result.ResponsesLimitReached, null);
            }


            var cachedSurvey = _cacheManager.Get(id);

            if (cachedSurvey != null)
            {
                //questionnaire = cachedSurvey.GetQuestionnaire();
                dictionary = cachedSurvey.GetQuestionnaireSet();
            }
            else
            {
                IGetDataFromDatabase data = new GetDataFromDatabase();
                if (!data.GetSurveySet(id, _context, out questionnaire, out dictionary))
                {
                    return new GetSurveyResult(Result.NotFound, null);
                }


                IGetSurveyParser parser = new GetSurveyParser();
                if (!parser.TryParseSurvey(questionnaire, dictionary, out json))
                {
                    return new GetSurveyResult(Result.Error, null);
                }

                cachedSurvey = new SurveyCache(questionnaire, dictionary, json);
                _cacheManager.Add(questionnaire.ID, cachedSurvey);
            }



            json = cachedSurvey.GetJson();

            return new GetSurveyResult(Result.Success, json);
        }

        private static bool IsTimeLimitReached(Questionnaire questionnaire)
        {
            if (!questionnaire.LimitDateTime.HasValue)
            {
                return false;
            }

            return questionnaire.LimitDateTime.Value < DateTime.Now;
        }

        private static bool IsResponsesLimitReached(Questionnaire questionnaire)
        {
            if (!questionnaire.ResponsesLimit.HasValue)
            {
                return false;
            }
            return questionnaire.Responses.Count >= questionnaire.ResponsesLimit;
        }
    }
}