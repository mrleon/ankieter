﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json.Linq;
using TestWeb.Models;

namespace TestWeb.Infrastructure.Survey.Get
{
    public class GetSurveyParser : IGetSurveyParser
    {
        public bool TryParseSurvey(Questionnaire questionnaire, IDictionary<Question, IList<Answer>> dictionary, out JObject json)
        {
            var jObject = new JObject { { "title", questionnaire.Title }, { "description", questionnaire.Description } };

            var questions = new JArray();
            foreach (var question in dictionary.OrderBy(t => t.Key.Index))
            {
                var jsonQuestion = new JObject
                {
                    {"title", question.Key.Query},
                    {"description", question.Key.Description},
                    {"type", question.Key.QuestionType.ToString()},
                    {"required", question.Key.Required},
                    {"index", question.Key.Index},
                    {"trySubmit", false}
                };

                var jsonAnswers = new JArray();
                if (HasOptions(question.Key.QuestionType))
                {
                    foreach (var answer in question.Value.OrderBy(t => t.Index))
                    {
                        jsonAnswers.Add(new JObject { { "content", answer.Content }, { "selected", false } });
                    }
                    jsonQuestion["options"] = jsonAnswers;
                }
                else if (question.Key.QuestionType == QuestionType.Text)
                {
                    for (var i = 0; i < question.Key.AnswerNumber; i++)
                    {
                        jsonAnswers.Add("");
                    }
                    jsonQuestion["options"] = jsonAnswers;
                }
                else
                {
                    jsonQuestion["content"] = "";
                }

                questions.Add(jsonQuestion);
            }

            jObject["forms"] = questions;

            json = jObject;
            return true;
        }

        private static bool HasOptions(QuestionType type)
        {
            return type == QuestionType.Checkbox || type == QuestionType.Radio;
        }
    }
}