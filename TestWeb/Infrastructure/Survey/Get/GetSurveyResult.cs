﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json.Linq;
using TestWeb.Infrastructure.Common;
using TestWeb.Infrastructure.Survey.Add;

namespace TestWeb.Infrastructure.Survey.Get
{
    public class GetSurveyResult : IGetSurveyResult
    {
        public Result Result { get; set; }

        public JObject Content { get; set; }

        public GetSurveyResult(Result result, JObject content)
        {
            Result = result;
            Content = content;
        }
    }
}