﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestWeb.Infrastructure.Survey.Get
{
    public interface IGetSurveyManager
    {
        IGetSurveyResult GetSurvey(int id);
    }
}
