﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using TestWeb.Models;

namespace TestWeb.Infrastructure.Survey.Get
{
    interface IGetSurveyParser
    {
        bool TryParseSurvey(Questionnaire questionnaire, IDictionary<Question, IList<Answer>> dictionary,
            out JObject json);
    }
}
