﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using TestWeb.Infrastructure.Common;
using TestWeb.Infrastructure.Survey.Add;

namespace TestWeb.Infrastructure.Survey.Get
{
    public interface IGetSurveyResult
    {
        Result Result { get; }

        JObject Content { get; }
    }
}
