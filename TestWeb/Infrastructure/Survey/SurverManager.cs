﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TestWeb.DAL;

namespace TestWeb.Infrastructure.Survey
{
    public abstract class SurverManager
    {
        protected readonly ExDbContext Context;

        protected SurverManager(ExDbContext context)
        {
            Context = context;
        }
    }
}