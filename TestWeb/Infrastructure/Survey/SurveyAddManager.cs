﻿//using System;
//using System.Collections.Generic;
//using System.Data.SqlClient;
//using System.Diagnostics;
//using System.Drawing;
//using System.Linq;
//using System.Web;
//using Microsoft.Ajax.Utilities;
//using Newtonsoft.Json.Linq;
//using TestWeb.DAL;
//using TestWeb.Infrastructure.Common;
//using TestWeb.Models;
//using WebGrease.Css.Extensions;

//namespace TestWeb.Infrastructure.Survey
//{
//    public class SurveyAddManager : SurverManager
//    {
//        private readonly string _title;
//        private readonly string _description;
//        private readonly string _content;
//        private readonly string _userId;

//        private Questionnaire questionnaire;

//        public SurveyAddManager(string title, string description, string content, ExDbContext context, string userId)
//            : base(context)
//        {
//            _title = title;
//            _description = description;
//            _content = content;
//            _userId = userId;
//        }

//        public bool TryParseAndAddSurvey()
//        {
//            if (!ValidateTitle(_title) || !ValidateDescription(_description))
//            {
//                Debug.WriteLine("Not valid title or description");
//                return false;
//            }

//            IList<Question> questions;

//            TryParse(out questions);
//            Debug.WriteLineIf(questions == null, "Questions parsing failed");
//            return questions != null && TryAdd(questions);
//        }

//        public int? GetId()
//        {
//            if (questionnaire != null)
//            {
//                return questionnaire.Id;
//            }

//            return null;
//        }

//        public string GetUrl()
//        {
//            return questionnaire.Url;
//        }

//        private static bool ValidateTitle(string title)
//        {
//            const int minimumlength = 5;
//            const int maximumlength = 100;

//            return !String.IsNullOrEmpty(title) && title.Length >= minimumlength && title.Length <= maximumlength;
//        }

//        private static bool ValidateDescription(string description)
//        {
//            const int maximumlength = 100;

//            return !String.IsNullOrWhiteSpace(description) && description.Length <= maximumlength;
//        }

//        private bool TryParse(out IList<Question> questions)
//        {
//            dynamic json = JArray.Parse(_content);

//            questions = new List<Question>();

//            try
//            {
//                foreach (var question in json)
//                {
//                    Question newQuestion = new Question();

//                    newQuestion.Required = question.required.Value;
//                    newQuestion.QuestionType = Enum.Parse(typeof(QuestionType), question.type.Value);
//                    newQuestion.Index = (int)question.index.Value;

//                    if (ValidateTitle(question.title.Value))
//                    {
//                        newQuestion.Query = question.title.Value;
//                    }
//                    else
//                    {
//                        throw new Exception("Not valid Title");
//                    }

//                    if (ValidateDescription(question.description.Value))
//                    {
//                        newQuestion.Description = question.description.Value;
//                    }
//                    else
//                    {
//                        throw new Exception("Not valid Description");
//                    }

//                    if (HasOptions(newQuestion.QuestionType) && TryParseOptions(question.options.ToString()))
//                    {
//                        newQuestion.Content = question.options.ToString();
//                    }
//                    else if (newQuestion.QuestionType == QuestionType.Text)
//                    {
//                        int numberOfOptions;
//                        if (Int32.TryParse(question.options.length, out numberOfOptions))
//                        {
//                            newQuestion.Content = numberOfOptions.ToString();
//                        }
//                    }
//                    else
//                    {
//                        throw new Exception("Not valid options");
//                    }

//                    questions.Add(newQuestion);
//                }
//            }
//            catch (Exception e)
//            {
//                Debug.WriteLine(e.Message);
//                Debug.WriteLine(e.StackTrace);
//                questions = null;
//                return false;
//            }


//            return true;
//        }

//        private bool TryAdd(IList<Question> questions)
//        {
//            try
//            {

//                questionnaire = new Questionnaire()
//                {
//                    ExUserId = _userId,
//                    Created = DateTime.Now,
//                    Title = _title,
//                    Description = _description,
//                    Url = _title.GetUrlFromTitle(),
//                };

//                Context.Questionnaires.Add(questionnaire);
//                Context.SaveChanges();

//                ListExtensions.ForEach(questions, t => t.QuestionnaireId = questionnaire.Id);
//                ListExtensions.ForEach(questions, t => t.ExUserId = _userId);
//                ListExtensions.ForEach(questions, t => Context.Questions.Add(t));

//                Context.SaveChanges();

//            }
//            catch (Exception e)
//            {
//                Debug.WriteLine(e.Message);
//                Debug.WriteLine(e.StackTrace);
//                return false;
//            }

//            return true;
//        }

//        private bool HasOptions(QuestionType type)
//        {
//            return type == QuestionType.Checkbox || type == QuestionType.Radio;
//        }

//        private bool TryParseOptions(string options)
//        {
//            const int minimumLength = 3;

//            dynamic json = JArray.Parse(options);

//            try
//            {
//                foreach (var option in json)
//                {
//                    if (String.IsNullOrWhiteSpace(option.Value) || option.Value.ToString().Length < minimumLength)
//                    {
//                        return false;
//                    }
//                }
//            }
//            catch (Exception e)
//            {
//                Debug.WriteLine(e.Message);
//                Debug.WriteLine(e.StackTrace);
//                return false;
//            }

//            return true;
//        }
//    }
//}