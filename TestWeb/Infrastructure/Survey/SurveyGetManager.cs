﻿//using System;
//using System.Collections.Generic;
//using System.Diagnostics;
//using System.Linq;
//using System.Security.Cryptography;
//using System.Web;
//using Newtonsoft.Json;
//using Newtonsoft.Json.Linq;
//using TestWeb.DAL;
//using TestWeb.Models;

//namespace TestWeb.Infrastructure.Survey
//{
//    public class SurveyGetManager : SurverManager
//    {
//        private string _json;
//        private Questionnaire _questionnaire;
//        private IList<Question> _questions;

//        public SurveyGetManager(ExDbContext context) : base(context) { }

//        public bool GetSurvey(int id)
//        {
//            _questionnaire = Context.Questionnaires.FirstOrDefault(t => t.Id == id);

//            if (_questionnaire == null)
//            {
//                return false;
//            }

//            _questions = Context.Questions.Where(t => t.QuestionnaireId == _questionnaire.Id).OrderBy(t => t.Index).ToList();

//            if (!TryParse())
//            {
//                return false;
//            }

//            return true;
//        }

//        public string GetTitle()
//        {
//            return _questionnaire.Title;
//        }

//        public string GetDescription()
//        {
//            return _questionnaire.Description;
//        }
//        public string GetJson()
//        {
//            return _json;
//        }

//        private bool TryParse()
//        {
            
//            var array = new List<object>();

//            foreach (var question in _questions)
//            {
//                array.Add(new
//                {
//                    query = question.Query,
//                    description = question.Description,
//                    options = JArray.Parse(question.Content),
//                    required = question.Required,
//                    index = question.Index,
//                    type = question.QuestionType.ToString()
//                });
//            }

//            try
//            {
//                _json = JsonConvert.SerializeObject(array);
//            }
//            catch (Exception e)
//            {
//                Debug.WriteLine(e.Message);
//                Debug.WriteLine(e.StackTrace);
//                return false;
//            }

//            return true;
//        }

//    }
//}