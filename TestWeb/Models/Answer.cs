﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestWeb.Models
{
    public class Answer
    {
        public int ID { get; set; }

        public int QuestionID { get; set; }

        public int Index { get; set; }

        public string Content { get; set; }

        public virtual Question Question { get; set; }
    }
}