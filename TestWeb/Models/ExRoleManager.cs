﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TestWeb.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using TestWeb.DAL;

namespace TestWeb.Models
{
    public class ExRoleManager : RoleManager<IdentityRole>
    {
        public ExRoleManager(IRoleStore<IdentityRole, string> roleStore)
            : base(roleStore)
        {
        }

        public static ExRoleManager Create(IdentityFactoryOptions<ExRoleManager> options, IOwinContext context)
        {
            return new ExRoleManager(new RoleStore<IdentityRole>(context.Get<ExDbContext>()));
        }
    }
}