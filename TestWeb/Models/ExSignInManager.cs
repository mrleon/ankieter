﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using TestWeb.App_Start;

namespace TestWeb.Models
{
    public class ExSignInManager : SignInManager<ExUser, string>
    {
        public ExSignInManager(ExUserManager userManager, IAuthenticationManager authenticationManager) :
            base(userManager, authenticationManager) { }

        public override Task<ClaimsIdentity> CreateUserIdentityAsync(ExUser user)
        {
            return user.GenerateUserIdentityAsync((ExUserManager)UserManager);
        }

        public static ExSignInManager Create(IdentityFactoryOptions<ExSignInManager> options, IOwinContext context)
        {
            return new ExSignInManager(context.GetUserManager<ExUserManager>(), context.Authentication);
        }
    }
}