﻿using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace TestWeb.Models
{
    public class ExUser : IdentityUser
    {
        public string Whatever { get; set; }

        public double Not2 { get; set; }


        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ExUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }
}