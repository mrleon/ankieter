﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using TestWeb.DAL;

namespace TestWeb.Models
{
    public class ExUserManager : UserManager<ExUser>
    {
        public ExUserManager(IUserStore<ExUser> store)
            : base(store)
        {
        }

        public static ExUserManager Create(IdentityFactoryOptions<ExUserManager> options, IOwinContext context)
        {
            var manager = new ExUserManager(new UserStore<ExUser>(context.Get<ExDbContext>()));

            manager.UserValidator = new UserValidator<ExUser>(manager)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = true,
            };

            manager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 6,
            };

            manager.MaxFailedAccessAttemptsBeforeLockout =
                Int32.Parse(System.Web.Configuration.WebConfigurationManager.AppSettings["MaxFailedAccessAttemptsBeforeLockout"]);

            manager.DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(5);




            return manager;
        }
    }
}