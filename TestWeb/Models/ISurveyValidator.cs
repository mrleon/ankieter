﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestWeb.Models
{
    interface ISurveyValidator
    {
        bool ValidateTitle(string title);
        bool ValidateDescription(string description);
    }
}
