﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestWeb.Models
{
    public class Question
    {
        public int ID { get; set; }

        public int QuestionnaireID { get; set; }

        public QuestionType QuestionType { get; set; }

        public int Index { get; set; }

        public string Query { get; set; }

        public string Description { get; set; }

        public int? AnswerNumber { get; set; }

        public bool Required { get; set; }


        public virtual Questionnaire Questionnaire { get; set; }

        public virtual IList<Answer> Answers { get; set; }

    }
}