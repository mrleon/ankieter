﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestWeb.Models
{
    public enum QuestionType
    {
        Radio,
        Checkbox,
        Text,
        Multiline
    }
}