﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace TestWeb.Models
{
    public class Questionnaire
    {
        public int ID { get; set; }

        public string ExUserID { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public DateTime Created { get; set; }

        public string Url { get; set; }

        public DateTime? LimitDateTime { get; set; }

        public int? ResponsesLimit { get; set; }


        public virtual ExUser ExUser { get; set; }

        public virtual IList<Question> Questions { get; set; }

        public virtual IList<Response> Responses { get; set; }

    }
}
