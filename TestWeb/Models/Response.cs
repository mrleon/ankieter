﻿using System;
using System.Collections.Generic;
using System.Security.Permissions;

namespace TestWeb.Models
{
    public class Response
    {
        public int ID { get; set; }

        public int QuestionnaireID { get; set; }

        public DateTime Created { get; set; }

        public virtual Questionnaire Questionnaire { get; set; }

        public virtual IList<ResponseValue> ResponseValue { get; set; }
    }
}