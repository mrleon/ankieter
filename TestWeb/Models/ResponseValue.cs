﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestWeb.Models
{
    public class ResponseValue
    {
        public int ID { get; set; }

        public int ResponseID { get; set; }

        public int QuestionIndex { get; set; }

        public string Value { get; set; }

        public virtual Response Response { get; set; }

    }
}