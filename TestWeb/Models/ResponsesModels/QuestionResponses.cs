﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestWeb.Models.ResponsesModels
{
    public class QuestionResponses : IEnumerable
    {
        public string Title { get; set; }
        public int Index { get; set; }
        public IDictionary<string, int> Dictionary { get; set; }

        public QuestionResponses(string title, int index, IDictionary<string, int> dictionary)
        {
            Title = title;
            Index = index;
            Dictionary = dictionary;
        }

        public IEnumerator GetEnumerator()
        {
            return Dictionary.GetEnumerator();
        }
    }
}