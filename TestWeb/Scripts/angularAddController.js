﻿angular.module('ankieter', [])
.controller('addController', ['$scope', '$http', '$location', function ($scope, $http, $location) {

    $scope.parent = { dateLimit: "" };
    $scope.isSending = false;
    $scope.formCounter = 0;
    $scope.errors = [];

    $scope.dateSelected = false;
    $scope.responsesSelected = false;

    $scope.setNewForm = function (option) {
        $scope.newForm = option;
    };

    $scope.addForm = function (option) {
        $scope.formCounter++;
        switch (option.value) {
            case 'Radio': {
                addRadioForm();
                break;
            }
            case 'Text': {
                addTextForm();
                break;
            }
            case 'Checkbox': {
                addCheckboxForm();
                break;
            }
            case 'Label': {
                addLabelForm();
                break;
            }
            case 'Multiline': {
                addMultilineTextForm();
                break;
            }
            case 'list': {
                addListForm();
                break;
            }
            case 'grid': {
                addGridForm();
                break;
            }
        }

    };

    $scope.submit = function () {
        var dateTime = "";
        var responsesLimit = 0;
        console.log($scope.dateSelected);
        console.log($scope.responsesSelected);
        if ($scope.dateSelected) {
            dateTime = $scope.parent.dateLimit;
        }
        if ($scope.responsesSelected) {
            responsesLimit = $scope.responsesLimit;
        }


        $scope.isSending = true;
        $http({
            method: "POST",
            url: window.addQuestionnaireUrl,
            data: {
                title: $scope.title,
                description: $scope.description,
                content: window.angular.toJson($scope.forms),
                dateLimit: dateTime,
                responsesLimit: responsesLimit
            }
        })
            .success(function (response) {
                if (response.result === "Success") {
                    window.location.href = response.url;

                } else {
                    console.log("Error occurred: " + response.result);
                }

            });
    };

    $scope.test = function () {
        $scope.errors = [];
        if (!isAnyFormRequired($scope.forms)) {
            $scope.errors.push("Przynajmniej jedno pytania musi być określone jako 'wymagane'.");
        }
        console.log($scope.errors.length);
        if ($scope.errors.length === 0) {
            $scope.submit();
        }
    };

    setInterval(function () { $scope.refreshDateLimitModel() }, 500);

    $scope.refreshDateLimitModel = function () {
        $scope.parent.dateLimit = $("#dateTimeModelId").val();
        $scope.$apply();
    };


    function addRadioForm() {
        var form =
        {
            "title": "",
            "description": "",
            "type": "Radio",
            "required": false,
            "index": $scope.formCounter,
            "options": [
                "",
                ""
            ]
        }
        $scope.forms.push(form);
    }
    function addCheckboxForm() {
        var form =
        {
            "title": "",
            "description": "",
            "type": "Checkbox",
            "required": false,
            "index": $scope.formCounter,
            "options": [
                ""
            ]
        }
        $scope.forms.push(form);
    }
    function addTextForm() {
        var form =
        {
            "title": "",
            "description": "",
            "type": "Text",
            "required": false,
            "index": $scope.formCounter,
            "options": [
                ""
            ]
        };
        $scope.forms.push(form);
    }
    function addLabelForm() {
        var form =
        {
            "title": "",
            "description": "",
            "type": "Label",
            "index": $scope.formCounter,
        };
        $scope.forms.push(form);
    }
    function addMultilineTextForm() {
        var form =
        {
            "title": "",
            "description": "",
            "type": "Multiline",
            "required": false,
            "index": $scope.formCounter,
        };
        $scope.forms.push(form);
    };

    $scope.removeForm = function (form) {
        for (var i = form.index; i < $scope.forms.length; i++) {
            $scope.forms[i].index--;
        }

        var index = $scope.forms.indexOf(form);
        $scope.forms.splice(index, 1);
        $scope.formCounter--;
    };

    $scope.changeOrder = function (oldIndex, newIndex) {
        if (oldIndex === newIndex) {
            return;
        }

        var form = $scope.forms[oldIndex];
        form.index = newIndex;

        for (var i = 0; i < $scope.forms.length; i++) {
            if ($scope.forms[i] === form)
                continue;
            if (newIndex < oldIndex) {
                if ($scope.forms[i].index >= newIndex && $scope.forms[i].index <= oldIndex) {
                    $scope.forms[i].index++;
                }
            }
            else {
                if ($scope.forms[i].index <= newIndex && $scope.forms[i].index >= oldIndex) {
                    $scope.forms[i].index--;
                }
            }
        }
        $scope.forms.sort(function (a, b) { return a.index - b.index; });
    };

    function isAnyFormRequired(forms) {
        for (var i = 0; i < forms.length; i++) {
            if (forms[i].required) {
                return true;
            }
        }
        return false;
    };






    $scope.forms = [
        {
            "title": "Jaka jest twoja płeć?",
            "description": "",
            "type": "Radio",
            "required": false,
            "index": 0,
            "options": [
                "Kobieta",
                "Mężczyzna",
                "Nie wiem"
            ]

        }
    ];

    $scope.options = [
     {
         "value": "Radio",
         "display": "Jednokrotny wybór"
     },
     {
         "value": "Text",
         "display": "Pole tekstowe"
     },
     {
         "value": "Checkbox",
         "display": "Wielokrotny wybór"
     },
     {
         "value": "Multiline",
         "display": "Pole tekstowe wielowierszowe"
     }
    ];


}])
.directive("checkboxForm", ["$sce", function ($sce) {

    return {
        restrict: "E",
        scope: {
            form: "=",
            formcount: "=",
            remove: "&",
            sort: "&"
        },
        templateUrl: window.checkboxAddDirectiveUrl,
        link: function (scope, element, attrs) {
            scope.removeOption = function (index, form) {
                form.options.splice(index, 1);
                console.log(form);
            };
            scope.addOption = function (form) {
                var option = "";
                form.options.push(option);
            };
            scope.rangeArray = function (range) {
                return new Array(range);
            };
        }
    }


}])
.directive('radioForm', ['$sce', function ($sce) {

    return {
        restrict: "E",
        scope: {
            form: "=",
            formcount: "=",
            remove: "&",
            sort: "&"
        },
        templateUrl: window.radioAddDirectiveUrl,
        link: function (scope, element, attrs) {
            scope.removeOption = function (index, form) {
                form.options.splice(index, 1);
            };
            scope.addOption = function (form) {
                var option = "";
                form.options.push(option);
                console.log(scope.formcount);
            };

            scope.validateTitle = function (title) {
                console.log("dupa");
                if (title.length < 3) {
                    return false;
                }
                return true;
            };

            scope.rangeArray = function (range) {
                return new Array(range);
            };
        }
    }


}])
.directive('textForm', ['$sce', function ($sce) {

    return {
        restrict: "E",
        scope: {
            form: "=",
            formcount: "=",
            remove: "&",
            sort: "&"
        },
        templateUrl: window.textAddDirectiveUrl,
        link: function (scope, element, attrs) {

            scope.removeOption = function (option, form) {
                var index = form.options.indexOf(option);
                form.options.splice(index, 1);
            };
            scope.addOption = function (form) {
                var option = "";
                form.options.push(option);
            };
            scope.rangeArray = function (range) {
                return new Array(range);
            };
        }
    }
}])
.directive('multilineForm', ['$sce', function ($sce) {

    return {
        restrict: "E",
        scope: {
            form: "=",
            formcount: "=",
            remove: "&",
            sort: "&"
        },
        templateUrl: window.multilineAddDirectiveUrl,
        link: function (scope, element, attrs) {
            scope.rangeArray = function (range) {
                return new Array(range);
            };
        }
    }
}]);