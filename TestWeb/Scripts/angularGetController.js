﻿angular.module('ankieter', [])
.controller('getController', ['$scope', '$http', '$location', function ($scope, $http, $location) {

    $scope.initValues = function (forms, title, description) {
        $scope.forms = JSON.parse(forms);
        $scope.title = title;
        $scope.description = description;
    };

    $scope.submit = function () {
        $http({
            method: "POST",
            url: window.addResponseUrl,
            data: {
                content: window.angular.toJson($scope.forms)
            }
        })
        .success(function (response) {
            window.location.href = response.url;
        });
    };

    $scope.test = function () {
        var result = true;
        var doSubmit = true;
        for (var i = 0; i < $scope.forms.length; i++) {
            var form = $scope.forms[i];
            form.trySubmit = true && form.required;
            switch (form.type) {
                case "Radio":
                    {
                        result = testRadio(form);
                        break;
                    }
                case "Checkbox":
                    {
                        result = testCheckbox(form);
                        break;
                    }
                case "Text":
                    {
                        result = testText(form);
                        break;
                    }
                case "Multiline":
                    {
                        result = testMultiline(form);
                        break;
                    }
            }
            console.log(result);
            if (!result) {
                doSubmit = false;
            }
        }
        console.log("end result:" + result);
        $scope.errors = [];
        if (doSubmit) {
            $scope.submit();
        } else {
            $scope.errors.push("Nie wszystkie wymagane pola zostały wypełnione");
        }
    };
}
])
.directive("checkboxForm", ["$sce", function ($sce) {

    return {
        restrict: "E",
        scope: {
            form: "="

        },
        templateUrl: window.checkboxDirectiveUrl,
        link: function (scope, element, attrs) {
            scope.test = function () {
                if (scope.form.required) {
                    for (var i = 0; i < scope.form.options.length; i++) {
                        if (scope.form.options[i].selected) {
                            return true;
                        }
                    }
                    return false;
                }
                return true;
            };
        }
    }


}])
.directive("radioForm", ["$sce", function ($sce) {

    return {
        restrict: "E",
        scope: {
            form: "="
        },
        templateUrl: window.radioDirectiveUrl,
        link: function (scope, element, attrs) {
            scope.radioChanged = function (option, form) {
                angular.forEach(form.options, function (c) {
                    c.selected = false;
                });
                option.selected = true;
            };
            scope.test = function () {
                if (scope.form.required) {
                    for (var i = 0; i < scope.form.options.length; i++) {
                        if (scope.form.options[i].selected) {
                            return true;
                        }
                    }
                    return false;
                }
                return true;
            };
        }
    }


}])
.directive("textForm", ["$sce", function ($sce) {
    return {
        restrict: "E",
        scope: {
            form: "="
        },
        templateUrl: window.textDirectiveUrl,
        link: function (scope, element, attrs) {
            scope.test = function () {
                if (scope.form.required) {
                    for (var i = 0; i < scope.form.options.length; i++) {
                        if (scope.form.options[i].trim().length > 0) {
                            return true;
                        }
                    }
                    return false;
                }
                return true;
            };
        }
    }
}])
.directive("multilineForm", ["$sce", function ($sce) {

    return {
        restrict: "E",
        scope: {
            form: "="
        },
        templateUrl: window.multilineDirectiveUrl,
        link: function (scope, element, attrs) {
            scope.test = function () {
                if (scope.form.required) {
                    if (scope.form.content.trim().length > 0) {
                        return true;
                    }
                    return false;
                }
                return true;
            };
        }
    }
}]);

var testCheckbox = function (form) {
    if (form.required) {
        for (var i = 0; i < form.options.length; i++) {
            if (form.options[i].selected) {
                return true;
            }
        }
        return false;
    }
    return true;
};

var testRadio = function (form) {
    if (form.required) {
        for (var i = 0; i < form.options.length; i++) {
            if (form.options[i].selected) {
                return true;
            }
        }
        return false;
    }
    return true;
};

var testText = function (form) {
    if (form.required) {
        for (var i = 0; i < form.options.length; i++) {
            if (form.options[i].trim().length > 0) {
                return true;
            }
        }
        return false;
    }
    return true;
};

var testMultiline = function (form) {
    if (form.required) {
        if (form.content.trim().length > 0) {
            return true;
        }
        return false;
    }
    return true;
};