﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestWeb.ViewModels
{
    public class MySurveyViewModel
    {
        public string Title { get; set; }

        public string Url { get; set; }

        public bool IsActive { get; set; }

        public int ResponsesAdded { get; set; }

        public DateTime? ClosedAtDateTime { get; set; }

        public int? ClosedAtResponsesNumber { get; set; }

        public string CsvUrl { get; set; }

        public string ReportUrl { get; set; }
    }
}