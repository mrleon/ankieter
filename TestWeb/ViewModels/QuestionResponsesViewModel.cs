﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestWeb.ViewModels
{
    public class QuestionResponsesViewModel
    {
        public string Title { get; set; }
        public string QuestionTypeDescription { get; set; }
        public bool IsRequired { get; set; }
        public int Index { get; set; }
        public IDictionary<string, int> Answers { get; set; }
    }
}