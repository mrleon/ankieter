﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestWeb.ViewModels
{
    public class SurveyResponsesViewModel
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public int ResponsesCount { get; set; }
        public IList<QuestionResponsesViewModel> Questions { get; set; }
    }
}